import * as React from "react"
import Seo from "../components/Seo"
export default function PageNotFound() {
  return (
    <>
      <Seo title="Pagina No Encontrada | Santitech" />
      <main className="page404">
        <section className="nes-container">
          <div className="message-list">
            <div class="message -left">
              <br />
              <i class="nes-ash"></i>
              <div class="nes-balloon from-left balloon">
                <h1>404</h1>
                <p>
                  Ups te acabas de perder, te recomiendo que vuelvas a la pagina
                  principal
                </p>
              </div>
            </div>
          </div>
          <a href="/">Volver</a>
        </section>
      </main>
    </>
  )
}
