import * as React from "react"
import Seo from "../components/Seo"
import Nav from "../components/Nav"
import Header from "../components/Header"
import Services from "../components/Services"
import Contact from "../components/Contact"
import Footer from "../components/Footer"

import "../styles/index.scss"
export default function Home() {
  return (
    <>
      <Seo />
      <Nav />
      <main className="main">
        <Header />
        <Services />
        <Contact />
      </main>
      <Footer />
    </>
  )
}
