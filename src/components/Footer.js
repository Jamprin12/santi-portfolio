import * as React from "react"

export default function Footer() {
  return (
    <footer className="footer">
      <div className="footer__nets">
        <ul>
          <h3>Contactame</h3>
          <li>
            <i className="nes-icon whatsapp is-medium"></i>
            <a href="/whatsapp">
              <b>Whatsapp</b>
            </a>
          </li>
          <li>
            <i className="nes-icon gmail is-medium"></i>
            <a href="/gamil">
              <b>Gmail</b>
            </a>
          </li>
        </ul>
      </div>
      <div className="footer__copyright">
        <span>Sitio Web creador por</span>
        <a href="https://coffejeancode.vercel.app">CoffeJeanCode</a>
      </div>
    </footer>
  )
}
