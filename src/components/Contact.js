import * as React from "react"

const Input = ({ type, id, placeholder, classes, textarea }) => (
  <div className={`nes-field ${classes}`}>
    <label htmlFor={id}>{placeholder}</label>
    {textarea ? (
      <textarea id={id} className="nes-textarea" required></textarea>
    ) : (
      <input type={type} id={id} className="nes-input" required />
    )}
  </div>
)
export default function Contact() {
  const handleSumbit = () => {}
  return (
    <section className="contact" id="contact">
      <h2>Contacto</h2>
      <form onSubmit={handleSumbit} className="contact__form">
        <Input
          id="name"
          type="text"
          placeholder="Nombre"
          classes="contact__input"
        />
        <Input
          id="email"
          type="email"
          placeholder="Email"
          classes="contact__input"
        />
        <Input
          id="message"
          placeholder="Mensaje"
          textarea
          classes="contact__input"
        />
      </form>
    </section>
  )
}
