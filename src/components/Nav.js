import * as React from "react"

const NavLink = ({ section, link }) => (
  <li className="nav__link">
    <a href={link}>{section}</a>
  </li>
)

export default function Nav() {
  React.useEffect(() => {
    const burger = document.querySelector(".nav__burger")
    const menu = document.querySelector(".nav__menu")

    burger.addEventListener("click", () => {
      menu.classList.toggle("active")
    })
    return () => {
      burger.removeEventListener("click", () => {
        burger.classList.remove("nav__burger")
      })
    }
  }, [])

  return (
    <nav className="nav">
      <div className="nav__logo">
        <h1>STech</h1>
      </div>
      <ul className="nav__menu">
        <NavLink section="servicios" link="#services" />
        <NavLink section="contacto" link="#contact" />
      </ul>
      <div className="nav__burger">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </nav>
  )
}
