import * as React from "react"
import { Title, Meta, Link } from "react-head"

export default function Seo({ title, description }) {
  return (
    <>
      <Title>{title ? title : "SantiTech"}</Title>
      <Meta name="description" content={description} />
      <Link rel="canonical" href="https://santitech.surge.sh" />
      <Meta name="description" content={description} />
      <Meta name="theme" content="#1d3557" />
      <Meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <Meta http-equiv="X-UA-Compatible" content="ie=edge" />
      <Meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
      <Meta name="keywords" content="santi, tech, arreglo, computadores" />
    </>
  )
}
