import * as React from "react"

export default function Header() {
  return (
    <header className="header">
      <h2>SantiTech</h2>
      <h4>Slogan</h4>
    </header>
  )
}
