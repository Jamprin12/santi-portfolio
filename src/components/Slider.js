import * as React from "react"
import SwiperCore, { A11y, Autoplay } from "swiper"
import { Swiper, SwiperSlide } from "swiper/react"

import "swiper/swiper.scss"

SwiperCore.use([A11y, Autoplay])

export default function Carousel() {
  return (
    <section className="carousel">
      <Swiper slidesPerView={1} autoplay={{ delay: 2000 }}>
        <SwiperSlide>Arreglar Computadoras</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
        <SwiperSlide>Slide 4</SwiperSlide>
      </Swiper>
    </section>
  )
}
