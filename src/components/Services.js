import * as React from "react"
import Slider from "./Slider"

export default function Services() {
  return (
    <section className="services" id="services">
      <h2>Mis Servicios</h2>
      <Slider />
    </section>
  )
}
